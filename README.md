## Testing McDonald's Subscribe feature with Selenium

### Group Members:

* Ajaydeep Singh - C0744219
* Amrik Singh - C0742318
* Preetwinder Kaur  - C0743856

### Test Cases:
* Title of the Subscription section is “Subscribe to my Mcd’s”.
* Subscribe using correct/valid input.
* Click the 'Subscribe' button with no data in input fields.
