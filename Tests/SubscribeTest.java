import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class SubscribeTest {

	//Location of Chromedriver file
			final String CHROMEDRIVER_LOCATION = "/Users/AJAY-MAC/chromedriver"; 
			
		// Website to be tested
			final String URL_TO_TEST = "http://www.mcdonalds.ca";
			
		//Global driver variables
			WebDriver driver;
		
		@Before
		public void setUp() throws Exception {
			
		// Selenium setup
			System.setProperty("webdriver.chrome.driver",CHROMEDRIVER_LOCATION);
			driver = new ChromeDriver();
			
		//Go to Website
			driver.get(URL_TO_TEST);
			
		// Exit the first window that offers to get McD's App
			WebElement closeWindowButton = driver.findElement(By.xpath("//a[@class = 'exit']"));
			closeWindowButton.click();
		}
		
		@After
	public void tearDown() throws Exception {
	//After you run the test case, close the browser
				Thread.sleep(10000);
				driver.close();
	}


	@Test
	public void subscriptionTitletest() throws InterruptedException {
	// Get the Title from Subscription Title using CssSelector
		WebElement SubscriptionTitle = driver.findElement(By.cssSelector(".form-container .default h2"));
		
	// Retrieving text from SubscriptionTitle
		String TitleText = SubscriptionTitle.getText();
		System.out.println(TitleText);
		
	// Check if expected output is equal to actual output
		assertEquals("Subscribe to my Mcd�s", TitleText);
		
	}
	
	@Test
	public void emailSignUpTest1() throws InterruptedException {
		
	//========================HAPPY PATH==================================//
		
	//Get the First Name Input Box and Enter First Name
		WebElement FirstNameInput = driver.findElement(By.cssSelector(".input-container #firstname2"));
		FirstNameInput.sendKeys("Ganesh Gaitonde");
		
	//Get the Email Input Box and Enter Email
		WebElement EmailInput = driver.findElement(By.cssSelector(".input-container #email2"));
		EmailInput.sendKeys("Gaitonde123@gmail.com");
				//Get the Postal code Input Box and Enter Postal Code
		WebElement PostalCodeInput = driver.findElement(By.cssSelector(".input-container #postalcode2"));
		PostalCodeInput.sendKeys("L6Y");
		
	//Get the Subscribe Button and Click it
		WebElement SubscribeButton = driver.findElement(By.cssSelector("#g-recaptcha-btn-2"));
		SubscribeButton.click();
		
	}
	
	@Test
	public void emailSignUpTest2() throws InterruptedException {
		//============================NEGATIVE CASE=================================//
		
		
	//Get the Subscribe Button and Click it
		WebElement SubscribeButton = driver.findElement(By.cssSelector("#g-recaptcha-btn-2"));
		SubscribeButton.click();
		
	//After you run the test case, close the browser
		//Thread.sleep(15000);
		//driver.close();
	
	}

}
